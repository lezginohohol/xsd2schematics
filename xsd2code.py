#!/usr/bin/env python3
'''
takes xsd file as parameter and creates py file with Schematics class definitions
'''

from lxml import etree as ET
from lxml import objectify
from pprint import pprint
from collections import OrderedDict
from xmljson import yahoo
import os


def readxml(xsdfile):
    try:
        parser = ET.XMLParser(ns_clean=True)
        xml = ET.parse(xsdfile, parser).getroot()
        print('Load', xsdfile)
        return xml
    except Exception:
        print('Failed load', xsdfile)

def clean_namespace(xml):
    for elem in xml.getiterator():
        if not hasattr(elem.tag, 'find'): continue  # (1)
        i = elem.tag.find('}')
        if i >= 0:
            elem.tag = elem.tag[i + 1:]
    objectify.deannotate(xml, cleanup_namespaces=True)
    for k in xml.attrib:
        if k.find('}')>0:
            del xml.attrib[k]


def remschem(tstr):
    return tstr.replace('{http://www.w3.org/2001/XMLSchema}', '')


def get_included_xsd_path(element, include):

    base_pathlist = element.base.split('/')
    base_pathlist = base_pathlist[:len(base_pathlist) - 1]  # xsd file name removed
    for level in include.split('/'):
        if level == '..':
            base_pathlist = base_pathlist[:len(base_pathlist) - 1]  # up one level
        else:
            base_pathlist.append(level)
    path = '/'.join(base_pathlist)
    return readxml(path)


def getstructure(xml, includeslist=None, complextypes=None, simpletypes=None, fullcomplextypes=None, fullsimpletypes=None):

    clean_namespace(xml)
    data = yahoo.data(xml)['schema']
    data['xmlfile_name'] = xml.base
    if includeslist:
        currincludeslist = includeslist[:]
    else:
        currincludeslist = []
    includes = []
    if 'include' not in data.keys():
        return data
    if isinstance(data['include'], OrderedDict):
        if currincludeslist.count((xml.base, data['include']['schemaLocation'])):
            return data
        currincludeslist.append((xml.base, data['include']['schemaLocation']))
        includes.append(getstructure(get_included_xsd_path(xml, data['include']['schemaLocation']), currincludeslist))
    elif isinstance(data['include'], list):
        for incl in data['include']:
            if currincludeslist.count((xml.base, incl['schemaLocation'])):
                continue
            currincludeslist.append((xml.base, incl['schemaLocation']))
            includes.append(getstructure(get_included_xsd_path(xml, incl['schemaLocation']), currincludeslist))
    data['includedstructures'] = includes
    return data


def prepare_lists(structure, objectdict):
    if 'includedstructures' in structure.keys():
        objectdict['fileslist'][structure['xmlfile_name']] = [s['xmlfile_name'] for s in structure['includedstructures']]
        for inclstruct in structure['includedstructures']:
            prepare_lists(inclstruct, objectdict)
    if 'complexType' in structure.keys():
        if isinstance(structure['complexType'], list):
            for ct in structure['complexType']:
                objectdict['complextypes'][ct['name']] = ct
        else:  # single object
            objectdict['complextypes'][structure['complexType']['name']] = structure['complexType']
    if 'simpleType' in structure.keys():
        if isinstance(structure['simpleType'], list):
            for st in structure['simpleType']:
                objectdict['simpletypes'][(structure['xmlfile_name'], st['name'])] = st
        else:  # single object
            objectdict['simpletypes'][(
                structure['xmlfile_name'],
                structure['simpleType']['name'])] = structure['simpleType']
    if 'element' in structure.keys():
        if isinstance(structure['element'], list):
            for ct in structure['element']:
                objectdict['elements'][(structure['xmlfile_name'], ct['name'])] = ct
        else:  # single object
            objectdict['elements'][(
                structure['xmlfile_name'],
                structure['element']['name'])] = structure['element']


xsdict = {
    'xs:int': 't.IntType',
    'xs:string': 't.StringType',
    'xs:dateTime': 't.DateTimeType',
    'xs:boolean': 't.BooleanType',
    'xs:double': 't.Float',
    'xs:date': 't.DateType'
}


def generate_validator(simpletype):
    st = simpletype
    validatorname = st['name']
    if 'pattern' in st['restriction'].keys():
        if isinstance(st['restriction']['pattern'], list):
            pattern = '|'.join([el['value'] for el in st['restriction']['pattern']])
        else:
            pattern = st['restriction']['pattern']['value']
        code = "def {0}(value):\n    if re.match('{1}', value):\n        return value\n    else:\n        " \
               "raise ValidationError(u'pattern match failed')\n".format(validatorname, pattern)
    if 'enumeration' in st['restriction'].keys():
        if isinstance(st['restriction']['enumeration'], list):
            enumeration = [st['restriction']['enumeration']]
        else:
            enumeration = st['restriction']['enumeration']
        code = "def is{0}(value):\n    if value in {1}:\n        return value\n    else:\n        " \
               "raise ValidationError(u'pattern match failed')\n".format(validatorname, enumeration)
    field_type = st['restriction']['base']
    if field_type[:3] == 'xs:':
        field_type = xsdict[field_type]
    return field_type, validatorname, code


def generate_field(attrtype, valdict):
    print(attrtype)
    if 'use' in attrtype.keys():
        if attrtype['use'] == 'required':
            use = 'required=True'
        else:
            use = ''
    else:
        use = ''
    if attrtype['type'][:3] == 'xs:':
        return "{0} = {1}({2})\n".format(attrtype['name'],
                                         xsdict[attrtype['type']],
                                         use)
    else:
        return '{0} = {1}(validators=[{2}],{3})\n'.format(
            attrtype['name'],                   # имя поля
            valdict[attrtype['type']]['type'],  # тип поля из таблицы валидаторов
            attrtype['type'],                   # имя типа с дополнительным валидатором
            use)                                # обязательность использования


def generate_model_field(modeldict, objectdict, is_sequence=False, is_complexcontent=False):
    if is_sequence:
        return '{0} = t.ListType(t.ModelType({1}Model)'.format(modeldict['name'], modeldict['type'])
    else:
        return '{0} = t.ModelType({1}Model)'.format(modeldict['name'], modeldict['type'])

def generate_model(complextype, objectdict):
    modelname = complextype['name']
    print(modelname)
    attrlines = []
    if 'attribute' in complextype.keys():
        if isinstance(complextype['attribute'], list):
            for attr in complextype['attribute']:
                attrlines.append(generate_field(attr, objectdict['validators']))
        else:
            attrlines.append(generate_field(complextype['attribute'], objectdict['validators']))

    if 'element' in complextype.keys():
        if isinstance(complextype['element'], list):
            for attr in complextype['element']:
                attrlines.append(generate_model_field(attr, objectdict))
        else:
            attrlines.append(generate_model_field(complextype['element'], objectdict))

    if 'sequence' in complextype.keys():
        if isinstance(complextype['sequence']['element'], list):
            for attr in complextype['sequence']['element']:
                attrlines.append(generate_model_field(attr, objectdict, True))
        else:
            attrlines.append(generate_model_field(complextype['sequence']['element'], objectdict, True))

    modeltext = 'class {0}Model(Model):\n'.format(modelname)
    for line in attrlines:
        modeltext = modeltext + '    ' + line
    modeltext = modeltext + '\n\n'
    return modeltext


def getpycode(objectdict):
    text = "#!/usr/bin/env python3\n\nimport schematics.types as t\nfrom betradar.models import Model\n" \
           "from schematics.exceptions import ValidationError\nimport re\n\n# Validators\n"
    for v in objectdict['validators']:
        text = text + objectdict['validators'][v]['code']
    text = text + "# Models\n"

    for ct in objectdict['complextypes']:
        text = text + generate_model(objectdict['complextypes'][ct], objectdict)
    return text


if __name__ == '__main__':
    xsdpath = 'xml/endpoints/unified'
    objectdict= {
        'fileslist': {},
        'complextypes': {},
        'simpletypes': {},
        'elements': {},
        'validators': {},
        'modelnames': {}
    }
    full_simple_types_list = {}
    for root, dirs, files in os.walk('{}/{}'.format(os.getcwd(), xsdpath)):
        for file in files:
            xml = readxml('/'.join((root, file)))
            structure = getstructure(xml)
            prepare_lists(structure, objectdict)
    print(pprint(objectdict))
    for simpletype in objectdict['simpletypes']:
        fieldType, validatorname, funccode = generate_validator(objectdict['simpletypes'][simpletype])
        objectdict['validators'][validatorname] = {'type': fieldType, 'code': funccode}

    pycode = getpycode(objectdict)
    with open('models.py', 'w') as fl:
        fl.write(pycode)
